/*
Padrão de nomeclatura das bases
Nome das Classes: camelCase sempre no plural
Campos: camelCase sempre no singular
Chaves estrangeiras: Nome da tabela camelCase + underscore + nome do campo camelCase na tabela de origem. Exemplo: tabelaCamelCase_nomeDoCampoNaTabelaDeOrigem
Tabelas de relações N:N: devem conter os nomes das tabelas + underscore Ex: clietes_hosts_filas
1 -  Os campos devem ser descretivios e não abreviados quando possível.
*/

-------------------------------------------------------------------------
--AQUI É O INICIO DA PARTE BÁSICA
-------------------------------------------------------------------------
/*
	ESSA TAABELA TEM A FUNÇÃO DE IDENTIFICAR OS CLIENTES NO REGISTROS
*/


--TABELA PARA A IDENTIFICAÇÃO DOS CLIENTES
DROP TABLE IF EXISTS customers;
CREATE TABLE customers (
    --ESTE CAMPO É 14 porque CNPJ tem 14 digitos e CPF 11
    id VARCHAR(14) NOT NULL PRIMARY KEY,
    name varchar(120),
    domain varchar(120)
);

--TABELA PARA IDENTIFICAÇÃO DOS MODULOS
DROP TABLE IF EXISTS modules;
CREATE TABLE modules (
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar (120),
    description text
);

--TABELA PRA IDENTIFICACAO DOS HOSTS
DROP TABLE IF EXISTS hosts;
CREATE TABLE hosts (
    id SERIAL NOT NULL PRIMARY KEY,
    name varchar(120),
    ip inet,
    hash varchar(32)
);

--TABELAS QUE RELACIONA CLIENTES MODULOS E HOSTS
DROP TABLE IF EXISTS customers_modules_hosts;
CREATE TABLE customers_modules_hosts (
    customers_id varchar(14) REFERENCES customers (id),
    modules_id integer REFERENCES modules (id),
    hosts_id integer REFERENCES hosts (id)
);



-- Visão das tabelas CUSTOMERS MODULES E HOSTS


DROP VIEW IF EXISTS view_customers_modules_hosts;

CREATE VIEW view_customers_modules_hosts AS (
	SELECT
		a2.name as cliente,
		a2.domain as dominio,
		CONCAT(a3.name, '.conf') as modulo,
		a3.description as decricao,
		a4.name as hostname,
		a4.ip,
		a4.hash as fs_uuid
	FROM 
		customers_modules_hosts as a1
	INNER JOIN
		customers a2 ON a1.customers_id = a2.id
	INNER JOIN
		modules a3 ON a1.modules_id = a3.id
	INNER JOIN
		hosts as a4 ON a1.hosts_id = a4.id
);

-------------------------------------------------------------------------
--AQUI É O FIM DA PARTE BÁSICA
-------------------------------------------------------------------------
